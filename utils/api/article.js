import request from "@/utils/request"

/**
 * 获取文章列表
 * @param {Object} params
 */
export function getArtcileList(params) {
	return request.get("article/list", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取文章详情内容
 * @param {Object} params
 */
export function getArticleContent(params) {
	return request.get("article/content", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取文章分类列表
 */
export function getArticleCateList() {
	return request.get("article/cate").then(res => {
		return res
	})
}

/**
 * 帮助文档分组列表
 * @param {Object} params
 */
export function getDocumentGroupList(params) {
	return request.get("dc/cate/list", {params:params}).then(res => {
		return res
	})
}

/**
 * 帮助文档内容详情
 * @param {Object} params
 */
export function getDocumentContent(params) {
	return request.get("dc/content", {params:params}).then(res => {
		return res
	})
}

/**
 * 提交文档点赞
 * @param {Object} params
 */
export function submitArticleCollect(params) {
	return request.post("article/collect", params).then(res => {
		return res
	})
}