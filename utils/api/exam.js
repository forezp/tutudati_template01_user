import request from "@/utils/request"

/**
 * 获取试卷分类
 * @param {Object} params
 */
export function getExamCategList(params) {
	return request.get("exam/cate/list", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取试卷列表
 * @param {Object} params
 */
export function getCollectionList(params) {
	return request.get("exam/collection/list", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取试卷详情
 * @param {Object} params
 */
export function getCollectionContent(params) {
	return request.get("exam/collection/content", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取试卷问答试题列表
 * @param {Object} params
 */
export function getReadingList(params) {
	return request.get("exam/reading/list", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 根据阅读题，获取到相关的试卷
 * @param {Object} params
 */
export function getReadingRelList(params) {
	return request.get("exam/collection/rel/list", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取试卷问答试题内容
 * @param {Object} params
 */
export function getReadingContent(params) {
	return request.get("exam/reading/content", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 查询试卷答题用户组列表
 * @param {Object} params
 */
export function getCollectionGroupList(params) {
	return request.get("exam/collection/groupList", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 提交试卷收藏记录
 * @param {Object} params
 */
export function submitCollection(params) {
	return request.post("exam/user/collection/collection", params).then(res => {
		return res
	})
}

/**
 * 查询问答答题用户组列表
 * @param {Object} params
 */
export function getReadingGroupList(params) {
	return request.get("exam/reading/groupList", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 提交问答试题收藏记录
 * @param {Object} params
 */
export function submitReadingCollection(params) {
	return request.post("exam/user/collection/reading", params).then(res => {
		return res
	})
}

/**
 * 获取选择试题
 * @param {Object} params
 */
export function getOptionList(params) {
	return request.get("exam/option/getList", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取当判断题
 * @param {Object} params
 */
export function getJudeList(params) {
	return request.get("exam/jude/getList", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取试卷完整试题
 * @param {Object} params
 */
export function getCollectionExamList(params) {
	return request.get("exam/collection/examList", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 提交试卷答题
 * @param {Object} params
 */
export function submitExamCollection(params) {
	return request.post("exam/collection/submit", params).then(res => {
		return res
	})
}

/**
 * 获取提交试卷结果信息
 * @param {Object} params
 */
export function getSubmitCollectionInfo(params) {
	return request.get("exam/collection/submit/info", {
		params: params
	}).then(res => {
		return res
	})
}

/**
 * 获取答题历史列表
 * @param {Object} params
 */
export function getSubmitHisotyList(params) {
	return request.get("exam/collection/submit/list", {params:params}).then(res => {
		return res
	})
}

/**
 * 获取答题历史排行
 * @param {Object} params
 */
export function getSubmitRankList(params) {
	return request.get("exam/collection/submit/rank/list", {params:params}).then(res => {
		return res
	})
}